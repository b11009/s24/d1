// Query Operators 

// [SECTION] Comparison Query Operators
// $gt and $gte operator (greater than / or equal to)

// find users with an age greater than 50
db.users.find({
	age: {
		$gt: 50
	}
})
// find users with an age greater than or equal to 50
db.users.find({
	age: {
		$gte: 50
	}
})

// find users with an age less than 50
db.users.find({
	age: {
		$lt: 50
	}
})
// find users with an age less than or equal to 50
db.users.find({
	age: {
		$lte: 50
	}
})

// find users with an age that is not equal 82
db.users.find({
	age: {
		$ne: 82
	}
})


// $in (looks one field)
// find users whose last names are either "Hawking"or "Doe"
db.users.find({
	lastName: {
		$in: ["Hawking", "Doe"] 
	}
})

db.users.find({
	courses: {
		$in: ["HTML", "React"]
	}
})


// [SECTION] Logical Query Operators
// $or operator (multiple fields)
db.users.find({
	$or: [{
		firstName: "Neil"
	},
	{
		age: 21
	}
	]
})

db.users.find( { 
	$or: [
		{ 
		lastName: "Hawking" 
		},
		{ 
		lastName:  "Armstrong" 
		}
	] 
})

// $and Operator
db.users.find({
	$and: [
		{
			age: {
				$ne: 82
			}
		},
		{
			age: {
				$ne: 76
			}
		}
	]
})

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 0,
		lastName: 0,
		contact: 0
	}
)
// Exlusion
/*Allows us to exlude or remove specific fields only when retrieving docs.
- the value provided is 0 to denote that the field is being icnluded.
Syntax:
db.users.find({criteria},{field:0})
*/
db.users.find(
	{
		firstName: "Jane"
	},
	{
		contact:0,
		department: 0
	}
);

// Suppresing the ID Field
 /* allows us to exclude the "_id" field when retrieving docs.
 When using field projection, field inclusion and exlusion may not be used at the same time.
 - Excluding the "_id" field is the only excpetion to this rule
 */
db.users.find( 
	{
		firstName: "Jane"
	},
	{
		firstName:1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
)

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 0,
		lastName: 0,
		contact: 0,
		_id: 1
	}
)

// Returning Specific Fields in Embedded Docs
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone":1
	}
)

// Supressing Specific Fields in Embedded Docs
db.users.find(
	{
		firstName: "Jane"
	},
	{
		"contact.phone":1
	}
);

// Project Specific Array Elements in the REturned array
// the $slice operator allows us to retrieve
// Only 1 element that matches the search criteria
db.users.insert({
	"namearr": [
		{namea: "juan"},
		{nameb: "tamad"}
	]
})

db.users.find(
	{ "namearr":
		{
			namea: "juan"
		}
	},
	{ namearr:
		{$slice: 1}
	}
)

db.users.insert({
	"fruitsarray": [
		{fruita: "apple"},
		{fruitb: "banana"},
		{fruitc: "strawberry"},
		{fruitd: "cherry"}
	]
})

db.users.find(
	{ "fruitsarray":
		{
			fruitb: "banana"
		}
	},
	{ fruitsarray:
		{$slice: 3}
	}
)

// $regex operator
/*
	-allows us to find docs that match a specific
	-string pattern using regular expressions

	Syntax:
	db.users.find({ field: $regex: 'pattern', $options: '$optionValue'});
*/

// Case sensitive query
db.users.find({firstName: {$regex: 'N'}});

// Case Insensitive query - $i
db.users.find({firstName: {$regex: 'j', $options: '$i'}});

db.users.find(
{
	$or: [
		{firstName: {$regex: 's', $options: '$i'}},
		{lastName: {$regex: 'd', $options: '$i'}}
	]
}, 
	{firstName: 1, lastName: 1, _id:0}
);




